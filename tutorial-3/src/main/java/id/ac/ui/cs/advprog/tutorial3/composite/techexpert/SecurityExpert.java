package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {

    public SecurityExpert(String name, double salary){
        if (salary < 70000.00){
            throw new IllegalArgumentException();
        }

        super.name = name;
        super.salary = salary;
        role = "Security Expert";

    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getRole() {
        return role;
    }
}
